#include "caf/actor_system.hpp"
#include "caf/actor_system_config.hpp"
#include "caf/behavior.hpp"
#include "caf/event_based_actor.hpp"
#include "caf/send.hpp"
#include "caf/stateful_actor.hpp"
#include <caf/all.hpp>
#include <chrono>

struct setActive {};
struct setInactive {};
struct red {};
struct yellow {};
struct green {};
struct redyellow {};

struct tlState {
  bool state = false;
  int redCounter = 0;
  int yellowCounter = 0;
  int greenCounter = 0;
  int redyellowCounter = 0;

  caf::actor lightActor;
};


caf::behavior light(caf::event_based_actor* self) {
  
  return caf::behavior {
    [self](red) {
      aout(self) << "Red" << std::endl;
    },
    
    [self](yellow) {
      aout(self) << "Yellow" << std::endl;
    },
    
    [self](green) {
      aout(self) << "Green" << std::endl;
    },

    [self](redyellow) {
      aout(self) << "Red, Yellow" << std::endl;
    }

  };
}

caf::behavior trafficLight(caf::stateful_actor<tlState>* self) {
  self -> state.lightActor = self -> home_system().spawn(light);
  self -> delayed_send(self, std::chrono::seconds{1}, yellow{}, 1); 

  return caf::behavior {
    [self](setActive) {
      self -> state.state = true;
      self -> delayed_send(self, std::chrono::seconds{1}, red{}); 
    },

    [self](setInactive) {
      self -> state.state = false;
      self -> delayed_send(self, std::chrono::seconds{1}, yellow{}, 1); 
    },

    [self](red) {
      ++self -> state.redCounter;
      self -> send(self -> state.lightActor, red{});
    
      if(self -> state.redCounter >= 30) {
        self -> state.redCounter = 0;
        self -> delayed_send(self, std::chrono::seconds{1}, redyellow{});
      }
      else self -> delayed_send(self, std::chrono::seconds{1}, red{});
    },

    [self](redyellow) {
      ++self -> state.redyellowCounter;
      self -> send(self -> state.lightActor, redyellow{});

      if(self -> state.redyellowCounter >= 2) {
        self -> state.redyellowCounter = 0;
        self -> delayed_send(self, std::chrono::seconds{1}, green{});
      }
      else self -> delayed_send(self, std::chrono::seconds{1}, redyellow{});
    },

    [self](yellow) {
      ++self -> state.yellowCounter;
      self -> send(self -> state.lightActor, yellow{});
    
      if(self -> state.yellowCounter >= 3) {
        self -> state.yellowCounter = 0;
        self -> delayed_send(self, std::chrono::seconds{1}, setActive{});
      }
      else self -> delayed_send(self, std::chrono::seconds{1}, yellow{});

    },
    
    [self](green) {
      ++self -> state.greenCounter;
      self -> send(self -> state.lightActor, green{});
    
      if(self -> state.greenCounter >= 30) {
        self -> state.greenCounter = 0;
        self -> delayed_send(self, std::chrono::seconds{1}, yellow{});
      }
      else self -> delayed_send(self, std::chrono::seconds{1}, green{});

    },

    [self](yellow, int) {
      if(!self -> state.state) self -> send(self -> state.lightActor, yellow{});
      if(!self -> state.state) self -> delayed_send(self, std::chrono::seconds{1}, yellow{}, 1); 
    },
  };
}

int main() {
  caf::actor_system_config cfg;
  caf::actor_system sys{cfg};
  
  caf::actor tl = sys.spawn(trafficLight);
  
  int num;
  while(std::cin >> num) {
    switch(num) {
      case 1:
        caf::anon_send(tl, setActive{});
        break;
      case 2:
        caf::anon_send(tl, setInactive{});
        break;
      default:
        break;
    }
  }

  return 0;
}
